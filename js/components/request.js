"use strict";

import { MainActions } from "./mainActions.js";
const LOGIN = "https://ajax.test-danit.com/api/v2/cards/login";
const API_URL = "https://ajax.test-danit.com/api/v2/cards";

let token = localStorage.getItem("userToken");
class Request {
  async getDani() {
    try {
      const email = document.getElementById("exampleInputEmail1").value;
      const password = document.getElementById("exampleInputPassword1").value;
      const response = await fetch(LOGIN, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({ email, password }),
      });

      if (!response.ok) {
        throw new Error("Network response was not ok");
      }

      token = await response.text();
      console.log("token", token);
      if (token) {
        localStorage.setItem("userToken", token);
        console.log("Token saved to localStorage:", token);
      } else {
        console.error("Token is undefined");
      }

      this.getCards();
    } catch (error) {
      console.error("Login error:", error);
    }
  }

  async postCard(card) {
    try {
      const response = await fetch(API_URL, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(card),
      });

      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      const data = await response.json();
      return data;
    } catch (error) {
      console.error("Post card error:", error);
    }
  }

  async deleteCard(id) {
    try {
      const response = await fetch(`${API_URL}/${id}`, {
        method: "DELETE",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
      });

      if (!response.ok) {
        throw new Error("Network response was not ok");
      }
      let savedCards = localStorage.getItem("cards");
      if (savedCards) {
        let cards = JSON.parse(savedCards);
        cards = cards.filter((card) => card.id !== id);
        localStorage.setItem("cards", JSON.stringify(cards));
      }
      return response;
    } catch (error) {
      console.error("Delete card error:", error);
    }
  }

  async putCard(card, id) {
    try {
      const { cardId, ...newCardEdit } = card;
      const response = await fetch(`${API_URL}/${id}`, {
        method: "PUT",
        headers: {
          "Content-Type": "application/json",
          Authorization: `Bearer ${token}`,
        },
        body: JSON.stringify(newCardEdit),
      });

      if (!response.ok) {
        throw new Error("Network response was not ok");
      }

      const data = await response.json();
      console.log(data);
      return data;
    } catch (error) {
      console.error("Update card error:", error);
    }
  }

  async getCards() {
    try {
      const response = await fetch(API_URL, {
        method: "GET",
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });

      if (!response.ok) {
        throw new Error("Network response was not ok");
      }

      const data = await response.json();

      localStorage.setItem("cards", JSON.stringify(data));
      let newActions = new MainActions();
      newActions.displayCards(data);
    } catch (error) {
      console.error("Fetch cards error:", error);
    }
  }
}

export { Request };
export { token };
