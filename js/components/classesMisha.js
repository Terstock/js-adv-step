import { Request } from "./request.js";
import { Modal } from "./modalAmina.js";
import { CardFilter } from "./filtersMax.js";
let mainDesk = document.querySelector(".card__container");
const modalEdit = document.querySelector(".modal__card--edit");

function getRightValue(card, domObject, key) {
  if (card) {
    card[key] = !card[key] ? "" : card[key];
    return { value: card[key] };
  } else {
    return domObject;
  }
}

class Visit {
  constructor(card) {
    this.inputPurpose = getRightValue(
      card,
      document.querySelector("#formGroupExampleInput"),
      "visitPurpose"
    );
    this.shortDescribe = getRightValue(
      card,
      document.querySelector("#formGroupExampleInput2"),
      "visitDescription"
    );
    this.inputEmergency = getRightValue(
      card,
      document.querySelector("#emergency-select"),
      "visitEmergency"
    );
    this.shortFullName = getRightValue(
      card,
      document.querySelector("#formGroupExampleInput3"),
      "visitShortInfo"
    );
    this.newVisit = getRightValue(
      card,
      document.querySelector("#newVisitInput"),
      "newVisitInfo"
    );
    this.statusVisit = getRightValue(
      card,
      document.querySelector("#emergency-select-new"),
      "statusVisitCard"
    );
    this.request = new Request();
    this.modal = new Modal();
    this.cardFilter = new CardFilter();
  }

  createCard() {
    this.visitInfo = {
      visitPurpose: this.inputPurpose.value,
      visitDescription: this.shortDescribe.value,
      visitEmergency: this.inputEmergency.value,
      visitShortInfo: this.shortFullName.value,
      newVisitInfo: this.newVisit.value,
      statusVisitCard: this.statusVisit.value,
    };
  }
  mainTextWork() {
    const mainText = document.querySelector(".main__text");
    let cards = localStorage.getItem("cards");
    if (cards) {
      cards = JSON.parse(cards);
      if (Array.isArray(cards) && cards.length > 0) {
        mainText.style.display = "none";
        console.log(cards);
      } else {
        mainText.style.display = "block";
      }
    } else {
      mainText.style.display = "none";
    }
  }
}

class VisitCardiologist extends Visit {
  constructor(card) {
    super(card);
    this.pressure = undefined;
    this.massBodyIndex = undefined;
    this.diseases = undefined;
    this.age = undefined;
    this.card = card;
  }

  createCard(token) {
    this.pressure = getRightValue(
      this.card,
      document.querySelector("#pressureInput"),
      "patientPressure"
    );
    this.massBodyIndex = getRightValue(
      this.card,
      document.querySelector("#massBodyIndex"),
      "patientMassBodyIndex"
    );
    this.diseases = getRightValue(
      this.card,
      document.querySelector("#diseases"),
      "patientDiseases"
    );
    this.age = getRightValue(
      this.card,
      document.querySelector("#age"),
      "patientAge"
    );
    super.createCard();

    const {
      visitPurpose,
      visitDescription,
      visitEmergency,
      visitShortInfo,
      newVisitInfo,
      statusVisitCard,
    } = this.visitInfo;

    const visitInfoCardiologist = {
      visitPurpose,
      visitDescription,
      visitEmergency,
      visitShortInfo,
      newVisitInfo,
      statusVisitCard,
      patientPressure: this.pressure.value,
      patientMassBodyIndex: this.massBodyIndex.value,
      patientDiseases: this.diseases.value,
      patientAge: this.age.value,
    };

    this.request
      .postCard(visitInfoCardiologist, token)
      .then((card) => {
        let savedCards = localStorage.getItem("cards");
        if (savedCards) {
          let cards = JSON.parse(savedCards);
          cards.push(card);
          localStorage.setItem("cards", JSON.stringify(cards));
        } else {
          localStorage.setItem("cards", JSON.stringify([card]));
        }
        this.renderCard(card, visitInfoCardiologist);
        const mainText = document.querySelector(".main__text");
        mainText.style.display = "none";
      })
      .catch((error) => {
        console.log(error);
      });
  }

  renderCard(card, visitInfoCardiologist) {
    let templateCard = document.querySelector(".template-card");
    let fragment = document.createDocumentFragment();
    const templateContent = templateCard.content.cloneNode(true);
    fragment.appendChild(templateContent);

    let cardFullName = fragment.querySelector(".card__name");
    cardFullName.textContent += card.visitShortInfo;

    let cardDoctor = fragment.querySelector(".card__doctor");
    cardDoctor.textContent += "Кардіолог";

    let deleteCard = fragment.querySelector(".card__button--close");
    deleteCard.addEventListener("click", () => {
      let cardContainer = deleteCard.closest(".card");
      this.request
        .deleteCard(card.id)
        .then((response) => {
          this.mainTextWork();
          console.log(response);
          cardContainer.remove();
        })
        .catch((error) => {
          console.log(error);
        });
    });
    let cardDiv = fragment.querySelector(".card");
    cardDiv.dataset.status = this.statusVisit.value;
    cardDiv.dataset.inputEmergency = this.inputEmergency.value;
    const infoObject = card ? card : visitInfoCardiologist;
    this.moreInfoCard(fragment, infoObject, cardDiv);
    this.cardEdit(infoObject, fragment, cardDoctor, card, cardFullName);
    mainDesk.append(cardDiv);
  }

  moreInfoCard(fragment, visitInfoCardiologist, cardDiv) {
    let showMoreCard = fragment.querySelector(".card__button--more");
    const cardFullInfo = document.createElement("div");
    let clickCount = 0;
    showMoreCard.addEventListener("click", () => {
      clickCount++;
      if (clickCount % 2 !== 0) {
        cardFullInfo.innerHTML = `<div>
        <span class="show-more-text"> Мета візиту: ${visitInfoCardiologist.visitPurpose}</span>
      <span class="show-more-text">Опис візиту: ${visitInfoCardiologist.visitDescription}</span>
      <span id="statusEmergencyId" class="show-more-text">Терміновість: ${visitInfoCardiologist.visitEmergency}</span>
      <span class="show-more-text">Дата візиту: ${visitInfoCardiologist.newVisitInfo}</span>
      <span id="statusVisitId" class="show-more-text">Статус візиту: ${visitInfoCardiologist.statusVisitCard}</span>
      <span class="show-more-text">Тиск: ${visitInfoCardiologist.patientPressure}</span>
      <span class="show-more-text">Індекс маси тіла: ${visitInfoCardiologist.patientMassBodyIndex}</span>
      <span class="show-more-text">Історія хвороб: ${visitInfoCardiologist.patientDiseases}</span>
      <span class="show-more-text">Вік: ${visitInfoCardiologist.patientAge}</span></div>`;
        cardDiv.append(cardFullInfo);
        showMoreCard.textContent = "Сховати";
      } else {
        cardFullInfo.remove();
        showMoreCard.textContent = "Показати більше";
      }
    });
  }

  cardEdit(visitInfoCardiologist, fragment, card, cardFullName) {
    let editCard = fragment.querySelector(".card__button--edit");
    editCard.addEventListener("click", () => {
      modalEdit.innerHTML = "";
      const secondFormTemplate = document.querySelector(
        ".second-form-template"
      );
      const cloneSecondForm = secondFormTemplate.content.cloneNode(true);
      const fragmentSecondForm = document.createDocumentFragment();
      fragmentSecondForm.append(cloneSecondForm);
      const secondForma = fragmentSecondForm
        .querySelector(".second-form")
        .cloneNode(true);
      secondForma.innerHTML = "";
      secondForma.appendChild(fragmentSecondForm.querySelector(".form-name"));
      secondForma.appendChild(
        fragmentSecondForm.querySelector(".form__close").cloneNode(true)
      );

      const divVisitPurpose = fragmentSecondForm
        .querySelector(".mb-3-1")
        .cloneNode(true);
      const inputVisitPurpose = divVisitPurpose.querySelector(
        "#formGroupExampleInput"
      );
      inputVisitPurpose.value = `${visitInfoCardiologist.visitPurpose}`;
      secondForma.appendChild(divVisitPurpose);

      const divVisitDescription = fragmentSecondForm
        .querySelector(".mb-3-2")
        .cloneNode(true);
      const inputVisitDescription = divVisitDescription.querySelector(
        "#formGroupExampleInput2"
      );
      inputVisitDescription.value = `${visitInfoCardiologist.visitDescription}`;
      secondForma.appendChild(divVisitDescription);

      const divVisitEmergency = fragmentSecondForm
        .querySelector(".mb-3-3")
        .cloneNode(true);
      const selectVisitEmergency =
        divVisitEmergency.querySelector("#emergency-select");
      selectVisitEmergency.value = `${visitInfoCardiologist.visitEmergency}`;
      secondForma.appendChild(divVisitEmergency);

      const divVisitShortInfo = fragmentSecondForm
        .querySelector(".mb-3-4")
        .cloneNode(true);
      const inputVisitShortInfo = divVisitShortInfo.querySelector(
        "#formGroupExampleInput3"
      );
      inputVisitShortInfo.value = `${visitInfoCardiologist.visitShortInfo}`;
      secondForma.appendChild(divVisitShortInfo);

      const divNewVisitInfo = fragmentSecondForm
        .querySelector(".mb-3-5")
        .cloneNode(true);
      const inputNewVisitInfo = divNewVisitInfo.querySelector("#newVisitInput");
      inputNewVisitInfo.value = `${visitInfoCardiologist.newVisitInfo}`;
      secondForma.appendChild(divNewVisitInfo);

      const divNewStatusVisit = fragmentSecondForm
        .querySelector(".mb-3-33")
        .cloneNode(true);
      const inputNewStatusVisit = divNewStatusVisit.querySelector(
        "#emergency-select-new"
      );
      inputNewStatusVisit.value = `${visitInfoCardiologist.statusVisitCard}`;
      secondForma.appendChild(divNewStatusVisit);

      let pressureDivNew = document.createElement("div");
      pressureDivNew.innerHTML = `<div class="mb-3">
      <label for="pressureInput-second" class="form-label">Ваш тиск:</label>
      <input type="text" class="form-control" id="pressureInput-second" placeholder="Будь ласка, введіть тиск:" value="${visitInfoCardiologist.patientPressure}" required>
    </div>`;
      let massBodyIndexDivNew = document.createElement("div");
      massBodyIndexDivNew.innerHTML = `<div class="mb-3">
            <label for="massBodyIndex" class="form-label">Ваш індекс маси тіла:</label>
            <input type="text" class="form-control" id="massBodyIndex" placeholder="Будь ласка, введіть індекс маси тіла:" value="${visitInfoCardiologist.patientMassBodyIndex}" required>
          </div>`;
      let diseasesDivNew = document.createElement("div");
      diseasesDivNew.innerHTML = `<div class="mb-3">
            <label for="diseases" class="form-label">Ваші перенесені захворювання:</label>
            <input type="text" class="form-control" id="diseases" placeholder="Перенесені захворювання серцево-судинної системи:" value="${visitInfoCardiologist.patientDiseases}" required>
          </div>`;
      let AgeDivNew = document.createElement("div");
      AgeDivNew.innerHTML = `<div class="mb-3">
            <label for="age" class="form-label">Ваш вік:</label>
            <input type="text" class="form-control" id="age" placeholder="Будь ласка, введіть повний вік:" value="${visitInfoCardiologist.patientAge}" required>
          </div>`;
      secondForma.appendChild(pressureDivNew);
      secondForma.appendChild(massBodyIndexDivNew);
      secondForma.appendChild(diseasesDivNew);
      secondForma.appendChild(AgeDivNew);
      secondForma.appendChild(
        fragmentSecondForm.querySelector(".mb-3-6").cloneNode(true)
      );

      modalEdit.style.display = "block";
      modalEdit.append(secondForma);

      let closeBtnModal = secondForma.querySelector("#close-create-visit");
      let btnNewCard = secondForma.querySelector("#create-card-btn");
      let btnSecondFormCLose = secondForma.querySelector(".form__close");
      let iconSecondFormCLose = secondForma.querySelector(".form__icon");
      secondForma.addEventListener("click", (e) => {
        if (
          e.target === closeBtnModal ||
          e.target === btnSecondFormCLose ||
          e.target === iconSecondFormCLose
        ) {
          modalEdit.style.display = "none";
          secondForma.style.display = "none";
        }
        if (e.target === btnNewCard) {
          e.preventDefault();
          btnNewCard.type = "submit";
          btnNewCard.setAttribute("data-bs-dismiss", "modal");
          modalEdit.style.display = "none";

          visitInfoCardiologist.visitPurpose = secondForma.querySelector(
            "#formGroupExampleInput"
          ).value;
          visitInfoCardiologist.visitDescription = secondForma.querySelector(
            "#formGroupExampleInput2"
          ).value;
          visitInfoCardiologist.patientPressure = secondForma.querySelector(
            "#pressureInput-second"
          ).value;
          visitInfoCardiologist.visitEmergency =
            secondForma.querySelector("#emergency-select").value;
          visitInfoCardiologist.visitShortInfo = secondForma.querySelector(
            "#formGroupExampleInput3"
          ).value;
          visitInfoCardiologist.newVisitInfo =
            secondForma.querySelector("#newVisitInput").value;
          visitInfoCardiologist.statusVisitCard = secondForma.querySelector(
            "#emergency-select-new"
          ).value;
          visitInfoCardiologist.patientMassBodyIndex =
            secondForma.querySelector("#massBodyIndex").value;
          visitInfoCardiologist.patientDiseases =
            secondForma.querySelector("#diseases").value;
          visitInfoCardiologist.patientAge =
            secondForma.querySelector("#age").value;

          const visitPurposeEdit = visitInfoCardiologist.visitPurpose;
          const visitDescriptionEdit = visitInfoCardiologist.visitDescription;
          const patientPressureEdit = visitInfoCardiologist.patientPressure;
          const visitEmergencyEdit = visitInfoCardiologist.visitEmergency;
          const visitShortInfoEdit = visitInfoCardiologist.visitShortInfo;
          const newVisitInfoEdit = visitInfoCardiologist.newVisitInfo;
          const newStatusVisitEdit = visitInfoCardiologist.statusVisitCard;
          const patientMassBodyIndexEdit =
            visitInfoCardiologist.patientMassBodyIndex;
          const patientDiseasesEdit = visitInfoCardiologist.patientDiseases;
          const patientAgeEdit = visitInfoCardiologist.patientAge;

          const newCardEdit = {
            cardId: card.id,
            visitPurpose: visitPurposeEdit,
            visitDescription: visitDescriptionEdit,
            patientPressure: patientPressureEdit,
            qvisitEmergency: visitEmergencyEdit,
            visitShortInfo: visitShortInfoEdit,
            newVisitInfo: newVisitInfoEdit,
            statusVisitCard: newStatusVisitEdit,
            patientMassBodyIndex: patientMassBodyIndexEdit,
            patientDiseases: patientDiseasesEdit,
            patientAge: patientAgeEdit,
          };

          cardFullName.textContent = `Пацієнт: ${newCardEdit.visitShortInfo}`;
          this.request
            .putCard(newCardEdit, card.id)
            .then((data) => {
              let savedCards = localStorage.getItem("cards");
              if (savedCards) {
                let cards = JSON.parse(savedCards);
                let editCardId = data.id;
                let editCard = cards.filter((visit) => visit.id !== editCardId);
                editCard.push(data);
                localStorage.setItem("cards", JSON.stringify(editCard));
              } else {
                localStorage.setItem("cards", JSON.stringify([data]));
              }
            })
            .catch((error) => {
              console.log(error);
            });
        }
      });
    });
  }
}

class VisitDentist extends Visit {
  constructor(card) {
    super(card);
    this.lastVisitDate = undefined;
    this.card = card;
  }

  createCard(token) {
    this.lastVisitDate = getRightValue(
      this.card,
      document.querySelector("#lastVisitInput"),
      "patientLastVisitDate"
    );

    super.createCard();
    const {
      visitPurpose,
      visitDescription,
      visitEmergency,
      visitShortInfo,
      statusVisitCard,
      newVisitInfo,
    } = this.visitInfo;

    const visitInfoDentist = {
      visitPurpose,
      visitDescription,
      visitEmergency,
      visitShortInfo,
      statusVisitCard,
      newVisitInfo,
      patientLastVisitDate: this.lastVisitDate.value,
    };

    this.request
      .postCard(visitInfoDentist, token)
      .then((card) => {
        let savedCards = localStorage.getItem("cards");
        if (savedCards) {
          let cards = JSON.parse(savedCards);
          cards.push(card);
          localStorage.setItem("cards", JSON.stringify(cards));
        } else {
          localStorage.setItem("cards", JSON.stringify([card]));
        }
        this.renderCard(card, visitInfoDentist);
        const mainText = document.querySelector(".main__text");
        mainText.style.display = "none";
      })
      .catch((error) => {
        console.log(error);
      });
  }
  renderCard(card, visitInfoDentist) {
    let templateCard = document.querySelector(".template-card");
    let fragment = document.createDocumentFragment();
    const templateContent = templateCard.content.cloneNode(true);
    fragment.appendChild(templateContent);

    let cardFullName = fragment.querySelector(".card__name");
    cardFullName.textContent += card.visitShortInfo;

    let cardDoctor = fragment.querySelector(".card__doctor");
    cardDoctor.textContent += "Стоматолог";

    let deleteCard = fragment.querySelector(".card__button--close");
    deleteCard.addEventListener("click", () => {
      let cardContainer = deleteCard.closest(".card");
      this.request
        .deleteCard(card.id)
        .then((response) => {
          this.mainTextWork();
          console.log(response);
          cardContainer.remove();
        })
        .catch((error) => {
          console.log(error);
        });
    });
    let cardDiv = fragment.querySelector(".card");
    cardDiv.dataset.status = this.statusVisit.value;
    cardDiv.dataset.inputEmergency = this.inputEmergency.value;
    const infoObject = card ? card : visitInfoDentist;
    this.moreInfoCard(fragment, infoObject, cardDiv);
    this.cardEdit(infoObject, fragment, cardDoctor, card, cardFullName);
    mainDesk.append(cardDiv);
  }
  moreInfoCard(fragment, visitInfoDentist, cardDiv) {
    let showMoreCard = fragment.querySelector(".card__button--more");
    const cardFullInfo = document.createElement("div");
    let clickCount = 0;
    showMoreCard.addEventListener("click", () => {
      clickCount++;
      if (clickCount % 2 !== 0) {
        cardFullInfo.innerHTML = `<span class="show-more-text"> Мета візиту: ${visitInfoDentist.visitPurpose}</span>
      <span class="show-more-text">Опис візиту: ${visitInfoDentist.visitDescription}</span>
      <span id="statusEmergencyId" class="show-more-text">Терміновість: ${visitInfoDentist.visitEmergency}</span>
      <span class="show-more-text">Дата візиту: ${visitInfoDentist.newVisitInfo}</span>
      <span id="statusVisitId" class="show-more-text">Статус візиту: ${visitInfoDentist.statusVisitCard}</span>
      <span class="show-more-text">Дата останнього візиту: ${visitInfoDentist.patientLastVisitDate}</span>`;
        cardDiv.append(cardFullInfo);
        showMoreCard.textContent = "Сховати";
      } else {
        cardFullInfo.remove();
        showMoreCard.textContent = "Показати більше";
      }
    });
  }
  cardEdit(visitInfoDentist, fragment, card, cardFullName) {
    let editCard = fragment.querySelector(".card__button--edit");
    editCard.addEventListener("click", () => {
      modalEdit.innerHTML = "";
      const secondFormTemplate = document.querySelector(
        ".second-form-template"
      );
      const cloneSecondForm = secondFormTemplate.content.cloneNode(true);
      const fragmentSecondForm = document.createDocumentFragment();
      fragmentSecondForm.append(cloneSecondForm);
      const secondForma = fragmentSecondForm
        .querySelector(".second-form")
        .cloneNode(true);
      secondForma.innerHTML = "";
      secondForma.appendChild(fragmentSecondForm.querySelector(".form-name"));
      secondForma.appendChild(
        fragmentSecondForm.querySelector(".form__close").cloneNode(true)
      );

      const divVisitPurpose = fragmentSecondForm
        .querySelector(".mb-3-1")
        .cloneNode(true);
      const inputVisitPurpose = divVisitPurpose.querySelector(
        "#formGroupExampleInput"
      );
      inputVisitPurpose.value = `${visitInfoDentist.visitPurpose}`;
      secondForma.appendChild(divVisitPurpose);

      const divVisitDescription = fragmentSecondForm
        .querySelector(".mb-3-2")
        .cloneNode(true);
      const inputVisitDescription = divVisitDescription.querySelector(
        "#formGroupExampleInput2"
      );
      inputVisitDescription.value = `${visitInfoDentist.visitDescription}`;
      secondForma.appendChild(divVisitDescription);

      const divVisitEmergency = fragmentSecondForm
        .querySelector(".mb-3-3")
        .cloneNode(true);
      const selectVisitEmergency =
        divVisitEmergency.querySelector("#emergency-select");
      selectVisitEmergency.value = `${visitInfoDentist.visitEmergency}`;
      secondForma.appendChild(divVisitEmergency);

      const divVisitShortInfo = fragmentSecondForm
        .querySelector(".mb-3-4")
        .cloneNode(true);
      const inputVisitShortInfo = divVisitShortInfo.querySelector(
        "#formGroupExampleInput3"
      );
      inputVisitShortInfo.value = `${visitInfoDentist.visitShortInfo}`;
      secondForma.appendChild(divVisitShortInfo);

      const divNewVisitInfo = fragmentSecondForm
        .querySelector(".mb-3-5")
        .cloneNode(true);
      const inputNewVisitInfo = divNewVisitInfo.querySelector("#newVisitInput");
      inputNewVisitInfo.value = `${visitInfoDentist.newVisitInfo}`;
      secondForma.appendChild(divNewVisitInfo);

      const divNewStatusVisit = fragmentSecondForm
        .querySelector(".mb-3-33")
        .cloneNode(true);
      const inputNewStatusVisit = divNewStatusVisit.querySelector(
        "#emergency-select-new"
      );
      inputNewStatusVisit.value = `${visitInfoDentist.statusVisitCard}`;
      secondForma.appendChild(divNewStatusVisit);

      let lastVisitDivNew = document.createElement("div");
      lastVisitDivNew.innerHTML = `<div class="mb-3">
            <label for="lastVisitInput" class="form-label">Останній візит:</label>
            <input type="datetime-local" class="form-control" id="lastVisitInput">
          </div>`;
      secondForma.appendChild(lastVisitDivNew);
      secondForma.appendChild(
        fragmentSecondForm.querySelector(".mb-3-6").cloneNode(true)
      );
      modalEdit.style.display = "block";
      modalEdit.append(secondForma);

      let closeBtnModal = secondForma.querySelector("#close-create-visit");
      let btnNewCard = secondForma.querySelector("#create-card-btn");
      let btnSecondFormCLose = secondForma.querySelector(".form__close");
      let iconSecondFormCLose = secondForma.querySelector(".form__icon");
      secondForma.addEventListener("click", (e) => {
        if (
          e.target === closeBtnModal ||
          e.target === btnSecondFormCLose ||
          e.target === iconSecondFormCLose
        ) {
          modalEdit.style.display = "none";
          secondForma.style.display = "none";
        }
        if (e.target === btnNewCard) {
          e.preventDefault();
          btnNewCard.type = "submit";
          btnNewCard.setAttribute("data-bs-dismiss", "modal");
          modalEdit.style.display = "none";

          visitInfoDentist.visitPurpose = secondForma.querySelector(
            "#formGroupExampleInput"
          ).value;
          visitInfoDentist.visitDescription = secondForma.querySelector(
            "#formGroupExampleInput2"
          ).value;
          visitInfoDentist.visitEmergency =
            secondForma.querySelector("#emergency-select").value;
          visitInfoDentist.visitShortInfo = secondForma.querySelector(
            "#formGroupExampleInput3"
          ).value;
          visitInfoDentist.newVisitInfo =
            secondForma.querySelector("#newVisitInput").value;
          visitInfoDentist.statusVisitCard = secondForma.querySelector(
            "#emergency-select-new"
          ).value;
          visitInfoDentist.patientLastVisitDate =
            secondForma.querySelector("#lastVisitInput").value;

          const visitPurposeEdit = visitInfoDentist.visitPurpose;
          const visitDescriptionEdit = visitInfoDentist.visitDescription;
          const visitEmergencyEdit = visitInfoDentist.visitEmergency;
          const visitShortInfoEdit = visitInfoDentist.visitShortInfo;
          const newVisitInfoEdit = visitInfoDentist.newVisitInfo;
          const statusVisitEdit = visitInfoDentist.statusVisitCard;
          const patientLastVisitDateEdit =
            visitInfoDentist.patientLastVisitDate;

          const newCardEdit = {
            cardId: card.id,
            visitPurpose: visitPurposeEdit,
            visitDescription: visitDescriptionEdit,
            qvisitEmergency: visitEmergencyEdit,
            visitShortInfo: visitShortInfoEdit,
            newVisitInfo: newVisitInfoEdit,
            statusVisitCard: statusVisitEdit,
            patientLastVisitDate: patientLastVisitDateEdit,
          };
          cardFullName.textContent = `Пацієнт: ${newCardEdit.visitShortInfo}`;
          this.request
            .putCard(newCardEdit, card.id)
            .then((data) => {
              let savedCards = localStorage.getItem("cards");
              if (savedCards) {
                let cards = JSON.parse(savedCards);
                let editCardId = data.id;
                let editCard = cards.filter((visit) => visit.id !== editCardId);
                editCard.push(data);
                localStorage.setItem("cards", JSON.stringify(editCard));
              } else {
                localStorage.setItem("cards", JSON.stringify([data]));
              }
            })
            .catch((error) => {
              console.log(error);
            });
        }
      });
    });
  }
}
class VisitTherapist extends Visit {
  constructor(card) {
    super(card);
    this.age2 = undefined;
  }
  createCard(token) {
    this.age2 = getRightValue(
      this.card,
      document.querySelector("#secondAge"),
      "patientAge"
    );

    super.createCard();
    const {
      visitPurpose,
      visitDescription,
      visitEmergency,
      visitShortInfo,
      newVisitInfo,
      statusVisitCard,
    } = this.visitInfo;

    const visistInfoTherapist = {
      visitPurpose,
      visitDescription,
      visitEmergency,
      visitShortInfo,
      newVisitInfo,
      statusVisitCard,
      patientAge: this.age2.value,
    };

    this.request
      .postCard(visistInfoTherapist, token)
      .then((card) => {
        let savedCards = localStorage.getItem("cards");
        if (savedCards) {
          let cards = JSON.parse(savedCards);
          cards.push(card);
          localStorage.setItem("cards", JSON.stringify(cards));
        } else {
          localStorage.setItem("cards", JSON.stringify([card]));
        }
        this.renderCard(card, visistInfoTherapist);
        const mainText = document.querySelector(".main__text");
        mainText.style.display = "none";
      })
      .catch((error) => {
        console.log(error);
      });
  }
  renderCard(card, visistInfoTherapist) {
    let templateCard = document.querySelector(".template-card");
    let fragment = document.createDocumentFragment();
    const templateContent = templateCard.content.cloneNode(true);
    fragment.appendChild(templateContent);
    let cardFullName = fragment.querySelector(".card__name");
    cardFullName.textContent += card.visitShortInfo;

    let cardDoctor = fragment.querySelector(".card__doctor");
    cardDoctor.textContent += "Терапевт";

    let deleteCard = fragment.querySelector(".card__button--close");
    deleteCard.addEventListener("click", () => {
      let cardContainer = deleteCard.closest(".card");
      console.log(card);
      this.request
        .deleteCard(card.id)
        .then((response) => {
          this.mainTextWork();
          console.log(response);

          cardContainer.remove();
        })
        .catch((error) => {
          console.log(error);
        });
    });
    let cardDiv = fragment.querySelector(".card");
    cardDiv.dataset.status = this.statusVisit.value;
    cardDiv.dataset.inputEmergency = this.inputEmergency.value;
    const infoObject = card ? card : visistInfoTherapist;
    this.moreInfoCard(fragment, infoObject, cardDiv);
    this.cardEdit(infoObject, fragment, cardDoctor, card, cardFullName);
    mainDesk.append(cardDiv);
  }
  moreInfoCard(fragment, visistInfoTherapist, cardDiv) {
    let showMoreCard = fragment.querySelector(".card__button--more");
    const cardFullInfo = document.createElement("div");
    let clickCount = 0;
    showMoreCard.addEventListener("click", () => {
      clickCount++;
      if (clickCount % 2 !== 0) {
        cardFullInfo.innerHTML = `
      <span class="show-more-text"> Мета візиту: ${visistInfoTherapist.visitPurpose}</span>
      <span class="show-more-text">Опис візиту: ${visistInfoTherapist.visitDescription}</span>
      <span id="statusEmergencyId" class="show-more-text">Терміновість: ${visistInfoTherapist.visitEmergency}</span>
      <span class="show-more-text">Дата візиту: ${visistInfoTherapist.newVisitInfo}</span>
      <span id="statusVisitId" class="show-more-text">Статус візиту: ${visistInfoTherapist.statusVisitCard}</span>
      <span class="show-more-text">Вік: ${visistInfoTherapist.patientAge}</span>`;

        cardDiv.append(cardFullInfo);
        showMoreCard.textContent = "Сховати";
      } else {
        cardFullInfo.remove();
        showMoreCard.textContent = "Показати більше";
      }
    });
  }
  cardEdit(visistInfoTherapist, fragment, cardDoctor, card, cardFullName) {
    let editCard = fragment.querySelector(".card__button--edit");
    editCard.addEventListener("click", () => {
      modalEdit.innerHTML = "";
      const secondFormTemplate = document.querySelector(
        ".second-form-template"
      );
      const cloneSecondForm = secondFormTemplate.content.cloneNode(true);
      const fragmentSecondForm = document.createDocumentFragment();
      fragmentSecondForm.append(cloneSecondForm);
      const secondForma = fragmentSecondForm
        .querySelector(".second-form")
        .cloneNode(true);
      secondForma.innerHTML = "";
      secondForma.appendChild(fragmentSecondForm.querySelector(".form-name"));
      secondForma.appendChild(
        fragmentSecondForm.querySelector(".form__close").cloneNode(true)
      );

      const divVisitPurpose = fragmentSecondForm
        .querySelector(".mb-3-1")
        .cloneNode(true);
      const inputVisitPurpose = divVisitPurpose.querySelector(
        "#formGroupExampleInput"
      );
      inputVisitPurpose.value = `${visistInfoTherapist.visitPurpose}`;
      secondForma.appendChild(divVisitPurpose);

      const divVisitDescription = fragmentSecondForm
        .querySelector(".mb-3-2")
        .cloneNode(true);
      const inputVisitDescription = divVisitDescription.querySelector(
        "#formGroupExampleInput2"
      );
      inputVisitDescription.value = `${visistInfoTherapist.visitDescription}`;
      secondForma.appendChild(divVisitDescription);

      const divVisitEmergency = fragmentSecondForm
        .querySelector(".mb-3-3")
        .cloneNode(true);
      const selectVisitEmergency =
        divVisitEmergency.querySelector("#emergency-select");
      selectVisitEmergency.value = `${visistInfoTherapist.visitEmergency}`;
      secondForma.appendChild(divVisitEmergency);

      const divVisitShortInfo = fragmentSecondForm
        .querySelector(".mb-3-4")
        .cloneNode(true);
      const inputVisitShortInfo = divVisitShortInfo.querySelector(
        "#formGroupExampleInput3"
      );
      inputVisitShortInfo.value = `${visistInfoTherapist.visitShortInfo}`;
      secondForma.appendChild(divVisitShortInfo);

      const divNewVisitInfo = fragmentSecondForm
        .querySelector(".mb-3-5")
        .cloneNode(true);
      const inputNewVisitInfo = divNewVisitInfo.querySelector("#newVisitInput");
      inputNewVisitInfo.value = `${visistInfoTherapist.newVisitInfo}`;
      secondForma.appendChild(divNewVisitInfo);

      const divNewStatusVisit = fragmentSecondForm
        .querySelector(".mb-3-33")
        .cloneNode(true);
      const inputNewStatusVisit = divNewStatusVisit.querySelector(
        "#emergency-select-new"
      );
      inputNewStatusVisit.value = `${visistInfoTherapist.statusVisitCard}`;
      secondForma.appendChild(divNewStatusVisit);

      let secondAgeDivNew = document.createElement("div");
      secondAgeDivNew.innerHTML = `<div class="mb-3">
            <label for="secondAge" class="form-label">Введіть повний вік:</label>
            <input type="text" class="form-control" id="secondAge" placeholder="Будь ласка, введіть повний вік:" required>
          </div>`;
      let secondAge = secondAgeDivNew.querySelector("#secondAge");
      secondAge.value = `${visistInfoTherapist.patientAge}`;
      secondForma.appendChild(secondAgeDivNew);
      secondForma.appendChild(
        fragmentSecondForm.querySelector(".mb-3-6").cloneNode(true)
      );

      modalEdit.style.display = "block";
      modalEdit.append(secondForma);

      let closeBtnModal = secondForma.querySelector("#close-create-visit");
      let btnNewCard = secondForma.querySelector("#create-card-btn");
      let btnSecondFormCLose = secondForma.querySelector(".form__close");
      let iconSecondFormCLose = secondForma.querySelector(".form__icon");
      secondForma.addEventListener("click", (e) => {
        if (
          e.target === closeBtnModal ||
          e.target === btnSecondFormCLose ||
          e.target === iconSecondFormCLose
        ) {
          modalEdit.style.display = "none";
          secondForma.style.display = "none";
        }
        if (e.target === btnNewCard) {
          e.preventDefault();
          btnNewCard.type = "submit";
          btnNewCard.setAttribute("data-bs-dismiss", "modal");
          modalEdit.style.display = "none";

          visistInfoTherapist.visitPurpose = secondForma.querySelector(
            "#formGroupExampleInput"
          ).value;
          visistInfoTherapist.visitDescription = secondForma.querySelector(
            "#formGroupExampleInput2"
          ).value;
          visistInfoTherapist.visitEmergency =
            secondForma.querySelector("#emergency-select").value;
          visistInfoTherapist.visitShortInfo = secondForma.querySelector(
            "#formGroupExampleInput3"
          ).value;
          visistInfoTherapist.newVisitInfo =
            secondForma.querySelector("#newVisitInput").value;
          visistInfoTherapist.statusVisitCard = secondForma.querySelector(
            "#emergency-select-new"
          ).value;
          visistInfoTherapist.patientAge =
            secondForma.querySelector("#secondAge").value;

          const visitPurposeEdit = visistInfoTherapist.visitPurpose;
          const visitDescriptionEdit = visistInfoTherapist.visitDescription;
          const visitEmergencyEdit = visistInfoTherapist.visitEmergency;
          const visitShortInfoEdit = visistInfoTherapist.visitShortInfo;
          const newVisitInfoEdit = visistInfoTherapist.newVisitInfo;
          const statusVisitEdit = visistInfoTherapist.statusVisitCard;
          const patientAgeEdit = visistInfoTherapist.patientAge;

          const newCardEdit = {
            cardId: card.id,
            visitPurpose: visitPurposeEdit,
            visitDescription: visitDescriptionEdit,
            qvisitEmergency: visitEmergencyEdit,
            visitShortInfo: visitShortInfoEdit,
            newVisitInfo: newVisitInfoEdit,
            statusVisitCard: statusVisitEdit,
            patientAge: patientAgeEdit,
          };

          cardFullName.textContent = `Пацієнт: ${newCardEdit.visitShortInfo}`;
          this.request
            .putCard(newCardEdit, card.id)
            .then((data) => {
              let savedCards = localStorage.getItem("cards");
              if (savedCards) {
                let cards = JSON.parse(savedCards);
                let editCardId = data.id;
                let editCard = cards.filter((visit) => visit.id !== editCardId);
                editCard.push(data);
                localStorage.setItem("cards", JSON.stringify(editCard));
              } else {
                localStorage.setItem("cards", JSON.stringify([data]));
              }
            })
            .catch((error) => {
              console.log(error);
            });
        }
      });
    });
  }
}

export { Visit };
export { VisitCardiologist };
export { VisitDentist };
export { VisitTherapist };
