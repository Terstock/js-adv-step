class CardFilter {
  buttonFilter() {
    let btnFilter = document.querySelector(".btn-filter");

    btnFilter.addEventListener("click", () => {
      const placeholderValue = document
        .querySelector(".filter__input")
        .value.trim()
        .toLowerCase();
      const status = document
        .querySelector("#main__filters-2")
        .value.trim()
        .toLowerCase();
      const importance = document
        .querySelector("#main__filters-3")
        .value.trim()
        .toLowerCase();
      const cards = document.querySelectorAll(".card");

      // console.log(cards);
      cards.forEach((card) => {
        let name = card
          .querySelector(".card__name")
          .innerText.trim()
          .toLowerCase();
        let statusAtribut = card.dataset.status.trim().toLowerCase();
        let atributEmergency = card.dataset.inputEmergency.trim().toLowerCase();

        let nameFilter =
          placeholderValue.length === 0 || name.includes(placeholderValue);
        let statusFilter =
          status === "усі-статус" || statusAtribut.includes(status);
        let emergencyFilter =
          importance === "усі-терміновість" ||
          atributEmergency.includes(importance);

        if (nameFilter && statusFilter && emergencyFilter) {
          card.style.display = "block";
        } else {
          card.style.display = "none";
        }
      });
    });
  }
}

const cardFilter = new CardFilter();
export { CardFilter };
