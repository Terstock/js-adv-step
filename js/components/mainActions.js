import { Visit, VisitCardiologist } from "./classesMisha.js";
import { VisitDentist } from "./classesMisha.js";
import { VisitTherapist } from "./classesMisha.js";
import { Modal } from "./modalAmina.js";
import { Request, token } from "./request.js";

class MainActions {
  displayCards(cards) {
    cards.forEach((card) => {
      if (card.patientPressure) {
        const visitCardiologist = new VisitCardiologist(card);
        visitCardiologist.renderCard(card);
      } else if (card.patientLastVisitDate) {
        const visitDentist = new VisitDentist(card);
        visitDentist.renderCard(card);
      } else if (card.patientAge) {
        const visitTherapist = new VisitTherapist(card);
        visitTherapist.renderCard(card);
      }
    });
  }

  loadCardsFromLocalStorage() {
    const savedCards = localStorage.getItem("cards");
    if (savedCards) {
      const cards = JSON.parse(savedCards);
      this.displayCards(cards);
    }
  }
}

document.addEventListener("DOMContentLoaded", () => {
  let modal = new Modal();
  let mainActions = new MainActions();
  const modalCreate = document.querySelector(".header__btn-modal-create");
  const btnHeader = document.querySelector(".header__btn--enter");
  let btnExit = document.querySelector(".btn-danger");

  if (token) {
    modal.basicCreate(token);
    mainActions.loadCardsFromLocalStorage();
    modalCreate.style.display = "block";
    btnExit.style.display = "block";
    btnHeader.style.display = "none";
  } else {
    console.log("No token found, login please...");
  }
});

export { MainActions };
