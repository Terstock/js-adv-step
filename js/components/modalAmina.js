`use strict`;

import { Request } from "./request.js";
import { Visit, VisitCardiologist } from "./classesMisha.js";
import { VisitDentist } from "./classesMisha.js";
import { VisitTherapist } from "./classesMisha.js";

let formTemplate = document.querySelector(".first-form-template");
const formTemplateContent = formTemplate.content.cloneNode(true);
let formFragment = document.createDocumentFragment();
formFragment.appendChild(formTemplateContent);
let startForm = document.querySelector(".form-select");
let modalBody = document.querySelector(".modal-body");
let formOne = formFragment.querySelector(".first-form").cloneNode(true);

const btnCloseForm = document.querySelector(".form__close");
const iconCloseForm = document.querySelector(".form__icon");
let btnExit = document.querySelector(".btn-danger");

class Modal {
  constructor() {
    this.newRequest = new Request();
  }
  createModal() {
    const btnHeader = document.querySelector(".header__btn--enter");
    const formAuthorization = document.querySelector(".form__authorization");

    btnHeader.addEventListener("click", () => {
      formAuthorization.style.overflow = "hidden";
      document.body.style.overflow = "hidden";
      formAuthorization.reset();
      formAuthorization.style.display = "block";
      formAuthorization.addEventListener("submit", (e) => {
        e.preventDefault();
        formAuthorization.style.display = "none";
        this.newRequest
          .getDani()
          .then((tokenServer) => {
            document.body.style.overflow = "auto";

            btnHeader.style.display = "none";
            const btnHeaderCreate = document.querySelector(
              ".header__btn-modal-create"
            );
            btnHeaderCreate.style.display = "block";
            this.basicCreate(tokenServer);
            btnExit.style.display = "block";
          })
          .catch((e) => {
            console.error(e);
            const alertError = document.querySelector(".modal__error");
            alertError.style.display = "block";
            const closeError = document.querySelector(".btn-close");
            const btnError = document.querySelector(".btn-secondary");
            window.addEventListener("click", (e) => {
              if (e.target === btnError || e.target === closeError) {
                alertError.style.display = "none";
                document.body.style.overflow = "auto";
              }
            });
          });
      });
    });
    window.addEventListener("click", (e) => {
      if (
        e.target === formAuthorization ||
        e.target === btnCloseForm ||
        e.target === iconCloseForm
      ) {
        formAuthorization.style.display = "none";
      }
    });
  }

  basicCreate(tokenServer) {
    let newVisit = new Visit();
    newVisit.mainTextWork();

    let startSelect = document.querySelector(".form-select");
    let btnClose = document.querySelector("#btn-modal-close");
    startSelect.addEventListener("change", (event) => {
      this.startFormWork(event);
    });
    btnClose.addEventListener("click", () => {
      this.closeVisit();
    });

    formOne.addEventListener("submit", (event) => {
      event.preventDefault();
      let presentInput = formOne.querySelector("#pressureInput");
      let presentInputTwo = formOne.querySelector("#lastVisitInput");
      let presentInputThree = formOne.querySelector("#secondAge");

      if (presentInput) {
        let visitCardiologist = new VisitCardiologist();
        visitCardiologist.createCard(tokenServer);
      }
      if (presentInputTwo) {
        let visitDentist = new VisitDentist();
        visitDentist.createCard(tokenServer);
      }
      if (presentInputThree) {
        let visitTherapist = new VisitTherapist();
        visitTherapist.createCard(tokenServer);
      }

      this.closeVisit();
    });
  }
  startFormWork(event) {
    formOne.innerHTML = "";
    formOne.append(
      formFragment.querySelector(".mb-3-1").cloneNode(true),
      formFragment.querySelector(".mb-3-2").cloneNode(true),
      formFragment.querySelector(".mb-3-3").cloneNode(true),
      formFragment.querySelector(".mb-3-4").cloneNode(true),
      formFragment.querySelector(".mb-3-5").cloneNode(true),
      formFragment.querySelector(".mb-3-33").cloneNode(true)
    );
    startForm.remove();

    switch (event.target.value) {
      case "1":
        let pressureDiv = document.createElement("div");
        pressureDiv.innerHTML = `<div class="mb-3">
            <label for="pressureInput" class="form-label">Ваш тиск:</label>
            <input type="text" class="form-control" id="pressureInput" placeholder="Будь ласка введіть Ваш тиск: " required>
          </div>`;
        let massBodyIndexDiv = document.createElement("div");
        massBodyIndexDiv.innerHTML = `<div class="mb-3">
            <label for="massBodyIndex" class="form-label">Ваш індекс маси тіла:</label>
            <input type="text" class="form-control" id="massBodyIndex" placeholder="Будь ласка введіть Ваш індекс маси тіла: " required>
          </div>`;
        let diseasesDiv = document.createElement("div");
        diseasesDiv.innerHTML = `<div class="mb-3">
            <label for="diseases" class="form-label">Ваші перенесені захворювання:</label>
            <input type="text" class="form-control" id="diseases" placeholder="Чи перенесли Ви якісь захворювання серцево-судинної системи? " required>
          </div>`;
        let AgeDiv = document.createElement("div");
        AgeDiv.innerHTML = `<div class="mb-3">
            <label for="age" class="form-label">Ваш вік:</label>
            <input type="text" class="form-control" id="age" placeholder="Будь ласка введіть Ваш вік: " required>
          </div>`;
        formOne.append(
          pressureDiv,
          massBodyIndexDiv,
          diseasesDiv,
          AgeDiv,
          formFragment.querySelector(".mb-3-6").cloneNode(true)
        );

        modalBody.append(formOne);
        break;
      case "2":
        let lastVisitDiv = document.createElement("div");
        lastVisitDiv.innerHTML = `<div class="mb-3">
            <label for="lastVisitInput" class="form-label">Ваш останній візит:</label>
            <input type="datetime-local" class="form-control" id="lastVisitInput" required>
          </div>`;
        formOne.append(
          lastVisitDiv,
          formFragment.querySelector(".mb-3-6").cloneNode(true)
        );
        modalBody.append(formOne);
        break;
      case "3":
        let secondAgeDiv = document.createElement("div");
        secondAgeDiv.innerHTML = `<div class="mb-3">
            <label for="secondAge" class="form-label">Введіть Ваш вік:</label>
            <input type="text" class="form-control" id="secondAge" placeholder="Будь ласка введіть Ваш вік:" required>
          </div>`;
        formOne.append(
          secondAgeDiv,
          formFragment.querySelector(".mb-3-6").cloneNode(true)
        );
        modalBody.append(formOne);
        break;
      default:
        break;
    }
    let crBtn = document.querySelector("#create-card-btn");
    crBtn.type = "submit";
    crBtn.setAttribute("data-bs-dismiss", "modal");
    let closeBtnModal = document.querySelector("#close-create-visit");
    closeBtnModal.setAttribute("data-bs-dismiss", "modal");
    closeBtnModal.addEventListener("click", () => {
      this.closeVisit();
    });
  }

  closeVisit() {
    modalBody.innerHTML = " ";
    modalBody.append(startForm);
  }

  deleteToken() {
    btnExit.addEventListener("click", () => {
      localStorage.removeItem("userToken");
      localStorage.removeItem("cards");
      location.reload();
    });
  }
}
export { Modal };
